import argparse

from oneuppuzzle_solver import Game


class Args(argparse.Namespace):
    game_definition: str


def _parse_args() -> Args:
    parser = argparse.ArgumentParser('oneuppuzzle-solver')
    parser.add_argument('game_definition', type=str)
    return parser.parse_args(namespace=Args())


def main():
    args = _parse_args()
    grid = Game(args.game_definition)
    grid.solve()
    if not grid.solved:
        print(
            f'Failed to solve grid without guessing after {grid.ticks} tries, '
            f'retrying with guesses. Grid:'
        )
        print(grid)
        grid = Game(args.game_definition)
        grid.solve_with_guesses()
        print(grid)
        print(grid.ticks)
    else:
        print(grid)
        print(grid.ticks)


if __name__ == '__main__':
    main()
