#!/usr/bin/env python3

from __future__ import annotations

import copy
import functools
import itertools
import operator
import pprint
import random
from collections import Counter
from typing import TYPE_CHECKING, NamedTuple, TypeVar

if TYPE_CHECKING:
    from collections.abc import Iterable

T = TypeVar('T')


def powerset(iterable: Iterable[T]) -> set[tuple[T, ...]]:
    s = list(iterable)
    return set(
        itertools.chain.from_iterable(itertools.combinations(s, r) for r in range(len(s) + 1))
    )


class Coordinate(NamedTuple):
    row: int
    col: int


class ColumnSection(NamedTuple):
    upper_wall_row: int
    lower_wall_row: int


class RowSection(NamedTuple):
    left_wall_col: int
    right_wall_col: int


class Cell:
    """
    Represents a single cell in the grid
    """

    def __init__(
        self,
        coord: Coordinate,
        possible_numbers: set[int],
        row_neighbor_coords: list[Coordinate],
        col_neighbor_coords: list[Coordinate],
        grid: Game,
    ):
        self.coord = coord
        self.possible_numbers = possible_numbers
        self.row_neighbor_coords = row_neighbor_coords
        self.col_neighbor_coords = col_neighbor_coords
        self.grid = grid

    @functools.cached_property
    def row_neighbors(self) -> list[Cell]:
        """
        NOTE: this function may not be called before the grid is completely initialized
        as that would break the cached property
        """
        return [self.grid.cells[coord.row][coord.col] for coord in self.row_neighbor_coords]

    @functools.cached_property
    def col_neighbors(self) -> list[Cell]:
        """
        NOTE: this function may not be called before the grid is completely initialized
        as that would break the cached property
        """
        return [self.grid.cells[coord.row][coord.col] for coord in self.col_neighbor_coords]

    @property
    def is_empty(self) -> bool:
        return len(self.possible_numbers) == 0

    @property
    def is_distinct(self) -> bool:
        return len(self.possible_numbers) == 1

    @property
    def is_ambiguous(self) -> bool:
        return len(self.possible_numbers) > 1

    @property
    def distinct_number(self) -> int | None:
        if self.is_distinct:
            return next(iter(self.possible_numbers))
        return None

    def validate(self) -> bool:
        """
        Checks whether the current set of possible numbers can legally exist within the grid.

        This only means that it may not be a distinct number that either already exists as a
        distinct number in one of its neighbors, or that the unique number is larger than the size
        of the section this cell lives in.
        """
        if self.is_empty:
            return False
        my_distinct_number = self.distinct_number
        if my_distinct_number is None:
            return True
        if my_distinct_number > min(
            len(self.row_neighbor_coords) + 1, len(self.col_neighbor_coords) + 1
        ):
            return False
        for neighbor in itertools.chain(self.row_neighbors, self.col_neighbors):
            distinct_neighbor_number = neighbor.distinct_number
            if distinct_neighbor_number is None:
                continue
            if distinct_neighbor_number == my_distinct_number:
                return False
        return True

    def _reduce_unique_subsets_in_section(self, powersets: dict[Cell, set[tuple[int, ...]]]):
        """
        If a possible_number set S of N numbers is a subset of the possible_number sets of exactly
        N cells in a section, and no other set in the section contains any numbers contained in S,
        you can remove all other numbers from the sets of those N cells that S is a subset of, as
        these N numbers have to be distributed among those N cells.

        Params
        ------
        powersets: dict[Cell, set[tuple[int, ...]]]
            A dict mapping a cell to the powerset of its possible_numbers set.
        """
        known_subsets = set()
        for ps in powersets.values():
            for subset in ps:
                if len(subset) == 0 or subset in known_subsets:
                    # we don't care about the empty subset or any subsets we have seen before
                    continue
                known_subsets.add(subset)
                cells_containing_subset = {
                    neighbor: ps for neighbor, ps in powersets.items() if subset in ps
                }
                if len(cells_containing_subset) == len(subset) and not any(
                    number in cell.possible_numbers
                    for number in subset
                    for cell in powersets
                    if cell not in cells_containing_subset
                ):
                    for neighbor in cells_containing_subset:
                        neighbor.possible_numbers = set(subset)

    def _reduce_by_distinctly_distributed_subsets(
        self, section_powerset: set[tuple[Cell, ...]], section_cells: list[Cell]
    ):
        """
        Let C be the set of cells in a section.
        Let S be a subset of C, and let N = |S|.
        If the union U of the possible_number sets of S contains exactly N elements, the numbers in
        U have to be distributed among S and may not appear in any other cell of C.

        This function tests all subsets of a given section C for those properties, and if it finds
        one such subset it removes the contained numbers from all other cells in C.

        Params
        ------
        section_powerset: set[tuple[Cell, ...]]
            The powerset of all the cells in the section.

        section_cells: list[Cell]
            A list of all the cells in the section.
        """
        distinctly_distributed_subsets = [
            cells_subset
            for cells_subset in section_powerset
            if len(cells_subset) > 0
            and len(
                functools.reduce(operator.or_, (cell.possible_numbers for cell in cells_subset))
            )
            == len(cells_subset)
        ]
        for subset in distinctly_distributed_subsets:
            distributed_numbers = functools.reduce(
                operator.or_, (cell.possible_numbers for cell in subset)
            )
            for cell in section_cells:
                if cell not in subset:
                    cell.possible_numbers -= distributed_numbers

    def reduce_complex(self) -> bool:
        """
        Reduces the amount of possible numbers based on the possible numbers of the cells neighbors.

        This uses a more sophisticated and complete approach than `reduce_simple` but is also much
        slower (a first simple test suggests a slowdown of about x10).

        Returns
        -------
        bool
            True if the number of possible numbers was reduced, else False.
        """
        # If only one number is possible we're already done
        if self.is_distinct:
            return False
        no_numbers_at_start = len(self.possible_numbers)

        row_section_powerset = powerset(self.row_neighbors + [self])
        col_section_powerset = powerset(self.col_neighbors + [self])
        self._reduce_by_distinctly_distributed_subsets(
            row_section_powerset, self.row_neighbors + [self]
        )
        self._reduce_by_distinctly_distributed_subsets(
            col_section_powerset, self.col_neighbors + [self]
        )

        row_powersets = {
            neighbor: powerset(neighbor.possible_numbers) for neighbor in self.row_neighbors
        } | {self: powerset(self.possible_numbers)}
        self._reduce_unique_subsets_in_section(row_powersets)
        col_powersets = {
            neighbor: powerset(neighbor.possible_numbers) for neighbor in self.col_neighbors
        } | {self: powerset(self.possible_numbers)}
        self._reduce_unique_subsets_in_section(col_powersets)

        return no_numbers_at_start != len(self.possible_numbers)

    def reduce_simple(self) -> bool:
        """
        Reduces the amount of possible numbers based on the possible numbers of the cells neighbors

        Returns
        -------
        bool
            True if the number of possible numbers was reduced, else False.
        """
        # If only one number is possible we're already done
        if self.is_distinct:
            return False
        no_numbers_at_start = len(self.possible_numbers)
        # Remove all numbers that are bound to be distributed among neighbors by their restrictions
        # (e.g. if two row-neighbors both only have possible sets {1,2}, we know that 1 and 2 for
        # the current row have to be distributed among those neighbors => we can't become 1 or 2).
        # This also subsumes the case that a neighbor has exactly one number in their set of
        # possible numbers.
        possible_numbers_in_row = Counter(
            frozenset(neighbor.possible_numbers) for neighbor in self.row_neighbors
        )
        possible_numbers_in_col = Counter(
            frozenset(neighbor.possible_numbers) for neighbor in self.col_neighbors
        )
        for possible_numbers, neighbor_amount in itertools.chain(
            possible_numbers_in_row.most_common(), possible_numbers_in_col.most_common()
        ):
            if len(possible_numbers) == neighbor_amount:
                self.possible_numbers -= possible_numbers
        # If we are the only cell in our row or column that has a particular number as a possible
        # number, we know that this number has to appear in this row or column, but since it can't
        # be in any of the other cells we know it has to be in our cell
        row_neighbors_possible_numbers = functools.reduce(
            operator.or_, possible_numbers_in_row.keys()
        )
        col_neighbors_possible_numbers = functools.reduce(
            operator.or_, possible_numbers_in_col.keys()
        )
        row_exclusively_possible = self.possible_numbers - row_neighbors_possible_numbers
        if row_exclusively_possible:
            self.possible_numbers = row_exclusively_possible
        col_exclusively_possible = self.possible_numbers - col_neighbors_possible_numbers
        if col_exclusively_possible:
            self.possible_numbers = col_exclusively_possible

        return no_numbers_at_start != len(self.possible_numbers)

    def __str__(self) -> str:
        return str(self.possible_numbers)

    def __repr__(self) -> str:
        return str(self)


class Game:
    def __init__(self, str_data: str):
        """
        Parameters
        ----------
        str_data : str
            The grid description as oneuppuzzle.com uses it
        """
        data = [int(c, 16) for c in str_data]
        self.size: int = data.pop(0)
        # indexed as cells[row][col]
        self.cells: list[list[Cell]] = []
        self.snapshot: list[list[Cell]] = []
        # indexed as row_sections[row][col]
        self.row_sections: list[list[RowSection]] = [[] for _ in range(self.size)]
        # indexed as col_sections[col][row]
        self.col_sections: list[list[ColumnSection]] = [[] for _ in range(self.size)]
        self.ticks: int = 0
        self.guessed = False
        self.parse_grid(data)

    @property
    def solved(self) -> bool:
        return all(len(cell.possible_numbers) == 1 for row in self.cells for cell in row)

    @property
    def valid(self) -> bool:
        for row_idx in range(len(self.cells)):
            for col_idx in range(len(self.cells[row_idx])):
                if not self.cells[row_idx][col_idx].validate():
                    return False
        return True

    def _store_snapshot(self):
        """
        "Backs up" the current grid cells to be restored at a later point.

        Thie is used when guessing, so that in case the guess leads to an invalid grid, we can
        restore the grid to a known valid and correct state.
        """
        self.snapshot = copy.deepcopy(self.cells)

    def _restore_snapshot(self):
        """
        Restores a "backed up" known valid state of the cells.
        """
        self.cells = self.snapshot
        self.snapshot = []
        self.guessed = False

    def _guess(self):
        """
        Picks a random cell from the grid, and assigns it a random possible number.
        Cell choice is weighted by the number of possible elements.
        The fewer elements are possible, the more likely is a cell to be chosen.
        """
        self.guessed = True
        min_possible_numbers = min(
            len(cell.possible_numbers) for cell in itertools.chain(*self.cells) if cell.is_ambiguous
        )
        relevant_cells = [
            cell
            for cell in itertools.chain(*self.cells)
            if len(cell.possible_numbers) == min_possible_numbers
        ]
        chosen_cell = random.choice(relevant_cells)
        chosen_cell.possible_numbers = {random.choice(list(chosen_cell.possible_numbers))}

    def parse_grid(self, data: list[int]):
        """
        Parses the grid representation in a format similar to the one used by oneuppuzzle.com,
        but using integers instead of hex digits.

        Params
        ------
        data: list[int]
            The grid given almost as it's used by oneuppuzzle.com, just with each character already
            interpreted as a hexadecimal number.
        """
        no_cells = self.size * self.size
        cells_data = data[:no_cells]
        walls_data = data[no_cells:]
        # parse walls and put into nice data structure
        vertical_walls_data = walls_data[::2]
        horizontal_walls_data = walls_data[1::2]
        vertical_walls_rows = [
            vertical_walls_data[i : i + self.size + 1] for i in range(0, no_cells, self.size + 1)
        ]
        horizontal_walls_cols = [
            [horizontal_walls_data[i + j * (self.size + 1)] for j in range(self.size + 1)]
            for i in range(self.size + 1)
        ]
        for row_idx, row in enumerate(vertical_walls_rows):
            for i in range(self.size):
                right_wall = row.index(1, i + 1)
                left_wall = (-row[::-1].index(1, -i - 1) - 1) % (self.size + 1)
                self.row_sections[row_idx].append(RowSection(left_wall, right_wall))
        for col_idx, row in enumerate(horizontal_walls_cols[:-1]):
            for i in range(self.size):
                lower_wall = row.index(1, i + 1)
                upper_wall = (-row[::-1].index(1, -i - 1) - 1) % (self.size + 1)
                self.col_sections[col_idx].append(ColumnSection(upper_wall, lower_wall))
        # parse cells and add wall restrictions
        cells_rows = [cells_data[i : i + self.size] for i in range(0, no_cells, self.size)]
        for row_idx, row in enumerate(cells_rows):
            self.cells.append([])
            for col_idx, cell_entry in enumerate(row):
                horizontal_section = self.row_sections[row_idx][col_idx]
                vertical_section = self.col_sections[col_idx][row_idx]
                # all columns in the section except for the column of our own cell shall become
                # neighbors
                row_neighbor_cols = set(
                    range(
                        horizontal_section.left_wall_col,
                        horizontal_section.right_wall_col,
                    )
                ) - {col_idx}
                # all rows in the section except for the row of our own cell shall become neighbors
                col_neighbor_rows = set(
                    range(vertical_section.upper_wall_row, vertical_section.lower_wall_row)
                ) - {row_idx}
                row_neighbors = [
                    Coordinate(row_idx, neighbor_col) for neighbor_col in row_neighbor_cols
                ]
                col_neighbors = [
                    Coordinate(neighbor_row, col_idx) for neighbor_row in col_neighbor_rows
                ]
                # cell_entry = 0 means the cell has no value pre-set for this grid, so we initialize
                # it with the possible numbers.
                # For initialization, we already take restrictions by walls surrounding the cell, as
                # well as the grid size into account.
                if cell_entry == 0:
                    possible_numbers = set(
                        range(
                            1,
                            min(
                                self.size,
                                horizontal_section.right_wall_col
                                - horizontal_section.left_wall_col,
                                vertical_section.lower_wall_row - vertical_section.upper_wall_row,
                            )
                            + 1,
                        )
                    )
                else:
                    possible_numbers = {cell_entry}
                coord = Coordinate(row_idx, len(self.cells[row_idx]))
                self.cells[row_idx].append(
                    Cell(coord, possible_numbers, row_neighbors, col_neighbors, self)
                )

    def tick(self) -> bool:
        """
        Iterates through each cell and tries to reduce its amount of possible numbers.
        First tries to only perform simple reduction steps.
        If no cell could be reduced but the grid is not solved, switch to the more complex reduction
        function for this tick.

        Returns
        -------
        bool
            True if at least one cell was changed, else False.
        """
        self.ticks += 1
        has_changed = False
        for cell in itertools.chain(*self.cells):
            has_changed |= cell.reduce_simple()
        if not has_changed and not self.solved:
            for cell in itertools.chain(*self.cells):
                has_changed |= cell.reduce_complex()
        return has_changed

    def solve(self) -> bool:
        """
        Keeps trying to reduce the amount of possible numbers in each cell until no more reductions
        are possible.

        Returns
        -------
        bool
            True if the grid is solved, else False
        """
        while self.tick():
            pass
        return self.solved

    def solve_with_guesses(self):
        """
        Tries to first solve the grid by only performing known-valid reductions.
        If no more reductions can be performed but the grid is not solved, backs-up this known valid
        grid and performs a single guess (choose a single number from the smallest ambiguous cell).
        Since the guess may be wrong, we now continuously check the grid for validity.
        Once we notice the grid got invalid, revert to the known correct grid and try to guess
        again.
        """
        while not self.solved:
            while self.tick():
                pass
            if not self.valid:
                self._restore_snapshot()
            elif not self.solved:
                if not self.guessed:
                    self._store_snapshot()
                self._guess()
            if not self.valid:
                self._restore_snapshot()

    def __str__(self) -> str:
        if self.solved:
            width = len(self.cells)
            top_row = ''.join(['┌'] + ['───┬'] * (width - 1) + ['───┐\n'])
            bottom_row = ''.join(['\n└'] + ['───┴'] * (width - 1) + ['───┘'])
            inbetween_rows = [
                ''.join(['├'] + ['───┼'] * (width - 1) + ['───┤']) for _ in range(width - 1)
            ]
            cell_rows = [
                '│ ' + ' │ '.join(str(next(iter(cell.possible_numbers))) for cell in row) + ' │'
                for row in self.cells
            ]
            mid_rows = '\n'.join(itertools.chain(*zip(cell_rows, inbetween_rows, strict=False)))[
                : -(4 * width + 2)
            ]
            return top_row + mid_rows + bottom_row
        return pprint.pformat(self.cells)

    def __repr__(self) -> str:
        return str(self)
