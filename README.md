# Oneuppuzzle solver

This is a solver for [oneuppuzzles](https://www.oneuppuzzle.com/).
It takes a game definition as used by the website as input and prints out a grid with the possible numbers for each cell (ideally just one per cell) as output.
This is not guaranteed to work, but the solving process will end when the solver has reduced the possible number per cell to the best of its capabilities, and so far has worked with all puzzles i have tested.

## How it works

The general idea of oneuppuzzles is explained [on the website](https://www.oneuppuzzle.com/help).
The solver saves a set of possible entries for each grid cell.
They are initialized based on the grid size, and the length of the section (cells between two walls) on the grid that the cell belongs to.
Afterwards, it iterates through each cell row by row from the top-left to bottom-right and tries to reduce the set of possible numbers of the cell based on the possible numbers of the neighboring cells (neighboring cells are defined as all the other cells in the same section, sometimes differentiated as "row neighbors" and "column neighbors" for the "row section" and "column section" respectively).

Reduction of possible numbers uses two methods.
The first method is to check whether sets of possible numbers of size `N` are shared between exactly `N` row- or column-neighbors (but not mixed row- *and* column-neighbors!).
If this is the case, we know that these numbers can not appear in the current cell, because they have to be distributed among the `N` neighbors in some way, as no duplicates are allowed, but each of these neighbors has to have one number assigned in the end.
Therefore, we remove all numbers of such sets from the cell's set of possible numbers.
This method also subsumes the case where a neighbor already has a single fixed number, as this means that there is exactly one neighbor with this set of size one.

The second method tests whether we have a single exclusive number in our set of possible numbers, that none of our either row- or column-neighbors contain in their sets of possible numbers.
If this is the case, we know that this number has to appear in the respective row- or column-section, but since none of our neighbors can end up with this number, it has to be our cell's number.
Similarly to the first method, this method could also be generalized for subsets of size `N` that are only shared between the current cell and `N-1` neighbors, where we could then remove all other numbers not in this subset from the set of possible numbers of the current cell.
However, this approach would be more complex to implement, and has thus be omitted for now.
